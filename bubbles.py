

# List the test scores
# Show the total quantity of scores tested
# Show the highest bubble score
# Show a list with the highest score solution indexes
# Show the most cost-effective bubble solution

scores = [60, 50, 60, 58, 54, 54,
58, 50, 52, 54, 48, 69,
34, 55, 51, 52, 44, 51,
69, 64, 66, 55, 52, 61,
46, 31, 57, 52, 44, 18,
41, 53, 55, 61, 51, 44]

 

costs = [.25, .27, .25, .25, .25, .25,
.33, .31, .25, .29, .27, .22,
.31, .25, .25, .33, .21, .25,
.25, .25, .28, .25, .24, .22,
.20, .25, .30, .25, .24, .25,
.25, .25, .27, .25, .26, .29]

 

length = len(scores)

max = 0

for i in range(length):
    # Imprimimos cada score de las pruebas
    print("Bubble solution #", str(i), "score", scores[i])

    #Buscamos el valor más alto en la lista y lo asignamos a max
    if scores[i] > max:
        max = scores[i]

best_solutions = []

for i in range(length):
    if max == scores[i]:
        best_solutions.append(i)

# Imprimimos el total de pruebas        
print(length)

# Imprimimos el resultado máximo de las pruebas
print("Highest bubble test:", str(max))

# Imprimimos la posición de los scores con el resultado máximo
print('Solutions with the highest score:',best_solutions)

# Calculate the most cost-effective bubble solution
# Pseudo codigo

# Crear variable flotante costo = 100
# Crear variable index_mas_efectivo = 0
# for i in range(length):
#   Si bubble solution en el score[i] es el maximo y bubble solution en el costs[i] es el minimo:
#       setear el valor de index_mas_efectivo al valor de i

min_cost = 100.0
index_mas_efectivo = 0

for i in range(length):
    if max == scores[i] and costs[i] < min_cost:
        min_cost = costs[i]
        index_mas_efectivo = i
print("La solucion mas efectiva tiene un costo de", min_cost, "Y se encuentra en la posicion ", index_mas_efectivo)
#print("Lowest bubble test cost:", str(min))